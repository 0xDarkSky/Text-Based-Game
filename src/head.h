#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define RESET "\x1B[0m"

void typewriter(char message[]) {
  int i = 0;

  while (message[i] != '\0') {
    printf("%c", message[i]);
    i++;
    fflush(stdout);
    usleep(60000);
  }
}