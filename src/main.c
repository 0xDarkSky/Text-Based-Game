#include "head.h"

struct variables {
  char choice[2];
  int option1, option2, option3, option4, option5, option6, option7;
};

void game();

int main() {
  struct variables v;
  system("clear");
  typewriter(RED "WELCOME TO THE GAME.\n\nWOULD YOU LIKE TO PLAY? Y/n " RESET);
  scanf("%c", v.choice);

  if (strcmp("y", v.choice) == 0) {
    typewriter("YOU HAVE CHOSEN TO PLAY THE GAME...\n");
    sleep(1);
    system("clear");
    game();
  }

  else {
    exit(0);
  }

  return 0;
}

void game() {
  struct variables x;
  typewriter("YOU WERE ASLEEP. YOU HAVE JUST WOKEN UP.\nYOU DON'T KNOW WHERE "
             "YOU ARE.\n");
  typewriter(RED "WHAT DO YOU DO?\n" RESET);
  printf("OPTIONS:\n[1] Look up\n[2] Look around\n[3] Yell for help\n");
  scanf("%d", &x.option1);

  switch (x.option1) {
  case 1:
    typewriter(
        "THE SKY IS BLUE. THE SUN IS BRIGHT. IT'S MOST LIKELY MORNING.\n\n");
    typewriter("YOU LOOK AROUND.\nYOU CAN SEE THE JUNGLE FAR AWAY. THERE'S A "
               "WATER POND NEARBY.\nTHERE IS AN OCEAN NEXT TO YOU.\n");
    break;
  case 2:
    typewriter("YOU LOOK AROUND.\nYOU CAN SEE THE JUNGLE FAR AWAY. THERE'S A "
               "WATER POND NEARBY.\nTHERE IS AN OCEAN NEXT TO YOU.\n\n");
    typewriter("YOU LOOK UP. THE SKY IS BLUE. THE SUN IS BRIGHT. IT'S MOST "
               "LIKELY MORNING.\n");
    break;
  case 3:
    typewriter("YOU YELL FOR HELP DESPERATELY.\nNOONE SEEMS TO HEAR YOU\n\n");
    typewriter("YOU LOOK AROUND. YOU CAN SEE THE JUNGLE FAR AWAY. THERE'S A "
               "WATER POND NEARBY. THERE IS AN OCEAN RIGHT NEXT TO YOU.\n");
    break;
  }

  typewriter(RED "\nWHAT DO YOU DO?\n" RESET);

  printf("OPTIONS:\n[1] Go to the jungle\n[2] Check out the water pond\n[3] "
         "Check the ocean\n");
  scanf("%d", &x.option2);

  switch (x.option2) {
  case 1:
    typewriter("YOU ENTER THE JUNGLE. THERE ARE MONKEYS JUMPING AROUND.\nTHEY "
               "HAVE NOTICED YOUR PRESENCE.\n");
    typewriter(RED "WHAT DO YOU DO?\n" RESET);
    printf("OPTIONS:\n[1] Go through the other path\n[2] Run away\n[3] Attack "
           "the monkeys\n");
    scanf("%d", &x.option3);

    if (x.option3 == 1) {
      typewriter("YOU CHOSE THE OTHER PATH.\nTHERE ARE PEOPLE WITH STICKS AND "
                 "STONES.\nTHEY SEEM AGGRESSIVE.\n");
      typewriter(RED "\nWHAT DO YOU DO?\n" RESET);

      printf("OPTIONS:\n[1] Befriend them\n[2] Attack them\n[3] Run away from "
             "them\n");
      scanf("%d", &x.option4);

      if (x.option4 == 1) {
        typewriter("YOU BEFRIEND THE WILD PEOPLE.\nYOU ADAPT TO THEM AND THEIR "
                   "LIFESTYLE.\nYOU SURVIVED.\n");
        typewriter(GREEN "YOU WON\n" RESET);
      }
      if (x.option4 == 2) {
        typewriter("YOU ATTACK THE WILD PEOPLE.\nTHERE ARE WAY TOO MANY OF "
                   "THEM AND THEY ARE ALL ARMED.\nYOU WERE BEATEN TO DEATH\n");
        typewriter(RED "GAME OVER\n" RESET);
      }
      if (x.option4 == 3) {
        typewriter("YOU RUN AWAY FROM THE WILD PEOPLE.\nYOU RUN BACK INTO THE "
                   "MONKEYS.\nTHEY ATTACK YOU AND BEAT YOU TO DEATH\n");
        typewriter(RED "GAME OVER\n" RESET);
      }
    }

    if (x.option3 == 2) {
      typewriter("YOU RUN AWAY FROM THE MONKIES AND FALL OFF A CLIFF\n");
      typewriter(RED "GAME OVER\n" RESET);
    }

    if (x.option3 == 3) {
      typewriter("YOU ATTACK THE MONKEYS.\nTHE MONKEYS HAVE OUTNUMBERED YOU. "
                 "YOU WERE BEATEN TO DEATH.\n");
      typewriter(RED "GAME OVER\n" RESET);
    }

    break;
  case 2:
    typewriter("YOU CHECK OUT THE WATER POND.\nYOU SUDDENLY REMEMBERED HOW "
               "THIRSTY YOU ARE.\n");
    typewriter(RED "\nWHAT DO YOU DO?\n" RESET);
    printf("OPTIONS:\n[1] Drink water\n[2] Look for fish\n");
    scanf("%d", &x.option4);
    if (x.option4 == 1) {
      typewriter("THE WATER SEEMS CLEAR.\nYOU DRINK SOME OF IT.\n");
      typewriter("...A FEW HOURS PASSED. YOUR STOMACH IS IN PAIN. YOU DIE.");
      typewriter(RED "GAME OVER." RESET);
    }
    if (x.option4 == 2) {
      typewriter("YOU ATTEMPT TO FIND FISH.\nYOU HAVE FOUND FISH.\nYOU CATCH "
                 "SOME SMALL FISHES.\n");
      typewriter(RED "WHAT DO YOU DO?\n" RESET);

      printf("OPTIONS:\n[1] Eat raw fish\n[2] Find a way to create fire\n");
      scanf("%d", &x.option5);

      if (x.option5 == 1) {
        typewriter("YOU EAT RAW FISH. YOU GOT POISONED AND DIED.\n");
        typewriter(RED "GAME OVER\n" RESET);
      }
      if (x.option5 == 2) {
        typewriter(
            "YOU ATTEMPT TO LIGHT UP FIRE WITH STICKS.\nAFTER MANY TRIES YOU "
            "SUCCEED.\nYOU COOK THE FISH AND EAT IT.\nTHE FISH HAS GIVEN YOU "
            "ABILITY TO DRINK WATER FROM THE POND.\nYOU SURVIVED\n");
        typewriter(GREEN "YOU WON\n" RESET);
      }
    }
    break;
  case 3:
    typewriter("YOU GO TO THE OCEAN\nTHERE ARE SHARKS IN THE SEA.\nTHERE ARE "
               "ALSO SMALL FISH IN THE SEA. YOU'RE HUNGRY.\n");
    typewriter(RED "\nWHAT DO YOU DO?\n" RESET);
    printf("OPTIONS:\n[1] Take the risk and go for the fish\n[2] Don't risk it "
           "and avoid the sharks\n");
    scanf("%d", &x.option6);
    if (x.option6 == 1) {
      typewriter("YOU ATTEMPT TO CATCH THE FISH AND SUCCEED.\nYOU MAKE A FIRE "
                 "PLACE AND COOK THE FISH.\nYOU SURVIVED.\n");
      typewriter(GREEN "YOU WON\n" RESET);
    }
    if (x.option6 == 2) {
      typewriter("YOU DON'T TAKE THE RISK. YOU'RE HUNGRY AND THIRSTY.\n");
      typewriter("...A FEW DAYS LATER YOU DIE.\n");
      typewriter(RED "GAME OVER\n" RESET);
    }
    break;
  }
}